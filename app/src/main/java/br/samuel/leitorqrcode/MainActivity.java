package br.samuel.leitorqrcode;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.zxing.BarcodeFormat;
import com.google.zxing.WriterException;
import com.google.zxing.common.BitMatrix;
import com.google.zxing.qrcode.QRCodeWriter;

public class MainActivity extends AppCompatActivity {
EditText edtQRCode;
ImageView imgQRCode;
Button btnQRCode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        btnQRCode.setOnClickListener(v -> {
            if (TextUtils.isEmpty(edtQRCode.getText().toString())){
                edtQRCode.setText("*");
                edtQRCode.requestFocus();
            } else{
                gerarQRCode(edtQRCode.getText().toString());
            }
        });
    }
    private void iniciarComponentes(){
        edtQRCode = findViewById(R.id.edtQRCode);
        btnQRCode = findViewById(R.id.btnQRCode);
        imgQRCode = findViewById(R.id.ivQRCode);
    }

    private void gerarQRCode(String conteudoQRCode){
       //zxing-android-embedded
        QRCodeWriter qrCodeWriter = new QRCodeWriter();
        try {
            BitMatrix bitMatrix = qrCodeWriter.encode(conteudoQRCode, BarcodeFormat.QR_CODE, 100, 100);
            int largura = bitMatrix.getWidth();
            int altura = bitMatrix.getHeight();
            Bitmap bitmap = Bitmap.createBitmap(largura, altura, Bitmap.Config.RGB_565);
            /* laço para desenhar o QRCode */
            for (int x = 0; x < largura; x++) {
                for (int y = 0; y < altura; y++) {
                    /* criando a imagem*/
                    bitmap.setPixel(x, y, bitMatrix.get(x,y) ? Color.BLACK : Color.WHITE);
                }
            }
            /* atribuindo ao ImageView*/
            imgQRCode.setImageBitmap(bitmap);
        }catch (WriterException e){
            e.printStackTrace();
        }
    }
}